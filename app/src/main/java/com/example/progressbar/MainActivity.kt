package com.example.progressbar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val handler: Handler = Handler()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Find  view By Id for ProgressBar,Button
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)
        val btn = findViewById<Button>(R.id.startProgress)
        val textViewProgress = findViewById<TextView>(R.id.textViewProgress)

        val progress = findViewById<ProgressBar>(R.id.progBar)

        //On Click for Button
        btn.setOnClickListener {
            var status = 0   //Initial status of Progress Bar

            Thread(Runnable {
                while (status <= 100) {
                    status += 1
                    handler.post { // handler for post text
                        progressBar.setProgress(status)
                        progress.setProgress(status)
                        textViewProgress.text = String.format("%d%%", status -1)
                    }
                    Thread.sleep(30) // for delay
                }
            }).start()
        }
//        img.setImageResource(R.drawable.ic_baseline_check_24)
    }
}

